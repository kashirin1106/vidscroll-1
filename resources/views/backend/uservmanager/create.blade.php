@extends ('backend.layouts.app')

@section ('title', "User Membership" . ' | ' . trans('labels.backend.pages.create'))

@section('page-header')
    <h1>
        User Video Manage
        <small>{{ trans('labels.backend.pages.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.videomanager.create', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission']) }}

    <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Create Page</h3>
                <div class="box-tools pull-right">
                     <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.videomanager')}}">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>back
                     </a>
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('user_id', "User Name", ['class' => 'col-lg-2 control-label required']) }}
 
                    <div class="col-lg-10">
                        <select class="col-lg-10 search-input-select form-control box-size" data-column = "1" name="user_id" required>
                        @if(!empty($all_users))
                            <option></option>
                            @foreach($all_users as $all_users_list)
                            <option value="{{$all_users_list->id}}">{{$all_users_list->name}}</option>
                            @endforeach
                        @endif    
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('video_title', "Video Title", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="video_title" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('video_url', "Video Url", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="video_url" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('video_notes', "Video Notes", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="video_notes" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('web_page_url', "WebSite Url", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="web_page_url" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="edit-form-btn">
                    {{ link_to_route('admin.user_membership', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                    <div class="clearfix"></div>
                </div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
    @endsection
@section("after-scripts")
    <script type="text/javascript">
        Backend.Pages.init();
    </script>
@endsection
