@extends ('backend.layouts.app')

@section ('title', "User Membership Management" . ' | ' . trans('labels.backend.pages.edit'))

@section('page-header')
    <h1>
        User Video Management
        <small>{{ trans('labels.backend.pages.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($user_video, ['route' => ['admin.videomanager.update', $user_video], 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'edit-role']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.pages.edit')}}</h3>

                <div class="box-tools pull-right">
                     <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.videomanager')}}">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>back
                     </a>
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('user_id', "User Name", ['class' => 'col-lg-2 control-label required']) }}
 
                    <div class="col-lg-10">
                        <input type="text" class="col-lg-10 form-control box-size" value="{{\App\Models\Membership\Usermembership::get_username($user_video->user_id)}}" readonly>
                        <input type="text" class="col-lg-10 form-control box-size hidden" name="user_id" value="{{$user_video->user_id}}" readonly>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('membership_id', "Video Title", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="video_title" value="{{$user_video->video_title}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('membership_id', "Video Url", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="video_url" value="{{$user_video->video_url}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('membership_id', "Video Notes", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="video_notes" value="{{$user_video->video_notes}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('membership_id', "WebSite Url", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="web_page_url" value="{{$user_video->web_page_url}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="edit-form-btn">
                    {{ link_to_route('admin.user_membership', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                    <div class="clearfix"></div>
                </div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
@endsection
@section("after-scripts")
    <script type="text/javascript">
        Backend.Pages.init();
    </script>
@endsection