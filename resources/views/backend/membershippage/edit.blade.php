@extends ('backend.layouts.app') 

@section ('title', "MembershipPage")

@section('page-header')
<h1>
    LandingPage
    <small>{{ trans('labels.backend.settings.edit') }}</small>
</h1>
@endsection 

@section('content') 
{{ Form::model($membershippage, ['route' => ['admin.membershippage.update'], 'class' => 'form-horizontal',
'role' => 'form', 'method' => 'POST','files' => true, 'id' => 'edit-landingpage']) }}

<div class="box box-info">
    <div class="box-header">
        <h3 class="box-title">{{ trans('labels.backend.settings.edit') }}</h3>
    </div>
    <div class="box-body setting-block">
        <!-- Nav tabs -->
        <ul id="myTab" class="nav nav-tabs setting-tab-list" role="tablist">
            <li role="presentation" class="active">
                <a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">Section1</a>
            </li>
            <li role="presentation">
                <a href="#tab2" aria-controls="1" role="tab" data-toggle="tab">Section2</a>
            </li>
            <li role="presentation">
                <a href="#tab3" aria-controls="2" role="tab" data-toggle="tab">Section3</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div id="myTabContent" class="tab-content setting-tab">
            <div role="tabpanel" class="tab-pane active" id="tab1">
                
                <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-8">
                        <textarea name="title1" class="form-control" placeholder="Title" rows="5">{{$membershippage->title1}}</textarea>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-lg-2 control-label">Explain</label>
                    <div class="col-lg-8">
                        <textarea name="explain1" class="form-control" placeholder="Title" rows="5">{{$membershippage->explain1}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('image1', 'Image', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">

                        <div class="custom-file-input">
                            {!! Form::file('image1', array('class'=>'form-control inputfile inputfile-1' )) !!}
                            <label for="image1">
                                <i class="fa fa-upload"></i>
                                <span>Choose a file</span>
                            </label>
                        </div>
                        
                    </div>
                    <!--col-lg-10-->
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab2">
                <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-8">
                        <textarea name="title2" class="form-control" placeholder="Title" rows="5">{{$membershippage->title2}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Explain</label>
                    <div class="col-lg-8">
                        <textarea name="explain2" class="form-control" placeholder="Explain" rows="5">
                            {{$membershippage->explain2}}
                        </textarea>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab3">
                <div class="form-group">
                    {{ Form::label('icon1', 'Image', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">

                        <div class="custom-file-input">
                            {!! Form::file('icon1', array('class'=>'form-control inputfile inputfile-1' )) !!}
                            <label for="icon1">
                                <i class="fa fa-upload"></i>
                                <span>Choose a file</span>
                            </label>
                        </div>
                        
                    </div>
                    <!--col-lg-10-->
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-8">
                        <input type="text" name="icon_title1" class="form-control" value="{{$membershippage->icon_title1}}" placeholder="Tip" row="2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Explain</label>
                    <div class="col-lg-8">
                        <textarea name="icon_explain1" class="form-control" placeholder="Explain" rows="5">
                            {{$membershippage->icon_explain1}}
                        </textarea>
                    </div>
                </div>
                <!-- 
                    icon2
                 -->
                 <div class="form-group">
                    {{ Form::label('icon2', 'Image', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">

                        <div class="custom-file-input">
                            {!! Form::file('icon2', array('class'=>'form-control inputfile inputfile-1' )) !!}
                            <label for="icon2">
                                <i class="fa fa-upload"></i>
                                <span>Choose a file</span>
                            </label>
                        </div>
                        
                    </div>
                    <!--col-lg-10-->
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-8">
                        <input type="text" name="icon_title2" class="form-control" value="{{$membershippage->icon_title2}}" placeholder="Tip" row="2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Explain</label>
                    <div class="col-lg-8">
                        <textarea name="icon_explain2" class="form-control" placeholder="Explain" rows="5">
                            {{$membershippage->icon_explain2}}
                        </textarea>
                    </div>
                </div>
                <!-- 
                icon3
                 -->
                 <div class="form-group">
                    {{ Form::label('icon3', 'Image', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">

                        <div class="custom-file-input">
                            {!! Form::file('icon3', array('class'=>'form-control inputfile inputfile-1' )) !!}
                            <label for="icon3">
                                <i class="fa fa-upload"></i>
                                <span>Choose a file</span>
                            </label>
                        </div>
                        
                    </div>
                    <!--col-lg-10-->
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-8">
                        <input type="text" name="icon_title3" class="form-control" value="{{$membershippage->icon_title3}}" placeholder="Tip" row="2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Explain</label>
                    <div class="col-lg-8">
                        <textarea name="icon_explain3" class="form-control" placeholder="Explain" rows="5">
                            {{$membershippage->icon_explain3}}
                        </textarea>
                    </div>
                </div>
            </div>
            
        
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-10 footer-btn">
                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div><!--box-->

<!-- hidden setting id variable -->
<input type="hidden" value="{{ $membershippage->id }}" id="membershippage" name="membershippage">
<input type="hidden" data-id="{{ $membershippage->id }}" id="setting">

{{ Form::close() }} 
@endsection 

@section('after-scripts')
<script src='/js/backend/bootstrap-tabcollapse.js'></script>
<!-- <script src="{{asset('js/backend/uploads.js')}}"></script> -->
<script>
    (function(){
        Backend.Utils.csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        Backend.Settings.selectors.RouteURL = "{{ route('admin.removeIcon', -1) }}";
        Backend.Settings.init();
        
    })();

    window.load = function(){
        
    }
    
    $('#myTab').tabCollapse({
        tabsClass: 'hidden-sm hidden-xs',
        accordionClass: 'visible-sm visible-xs'
    });
    $('.inputfile').bind('change', function() {

      //this.files[0].size gets the size of your file.
      if(this.files[0].size > 4194304)
      {

        alert("Filesize have to be small that 4 Megabytes");

      }

    });
</script>
@endsection