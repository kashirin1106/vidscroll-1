@extends ('backend.layouts.app')

@section ('title', "User Membership Management" . ' | ' . trans('labels.backend.pages.edit'))

@section('page-header')
    <h1>
        User Membership Management
        <small>{{ trans('labels.backend.pages.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($user_membership, ['route' => ['admin.user_membership.update', $user_membership], 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'edit-role']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.pages.edit')}}</h3>

                <div class="box-tools pull-right">
                     <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.user_membership')}}">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>back
                     </a>
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('user_id', "Name", ['class' => 'col-lg-2 control-label required']) }}

                    <div class="col-lg-10">
                        <input type="text" class ="form-control box-size" value="{{\App\Models\Membership\Usermembership::get_username($user_membership->user_id)}}" readonly>
                        <input type="text" name="user_id" class ="form-control box-size hidden" value="{{$user_membership->user_id}}" readonly>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('membership_id', "Membership", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <select class="col-lg-10 search-input-select form-control box-size" data-column = "1" name="membership_id" required>
                        @if(!empty($memberships))
                            <option></option>
                            @foreach($memberships as $memberships_list)
                            <option value="{{$memberships_list->id}}" @if($memberships_list->id == $user_membership->membership_id) selected @endif>{{$memberships_list->name}}</option>
                            @endforeach
                        @endif    
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('start_at', "Start Date", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input type="date" name= "start_at" class ="form-control box-size" value="{{$user_membership->start_at}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('end_at', "End Date", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input type="date" name= "end_at" class ="form-control box-size" value="{{$user_membership->end_at}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="edit-form-btn">
                    {{ link_to_route('admin.user_membership', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                    <div class="clearfix"></div>
                </div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
@endsection
@section("after-scripts")
    <script type="text/javascript">
        Backend.Pages.init();
    </script>
@endsection