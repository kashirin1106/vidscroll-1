@extends ('backend.layouts.app')

@section ('title', "User Membership")

@section('page-header')
    <h1>Membership</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">User Membership</h3>

            <div class="box-tools pull-right">
                <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.user_membership.add')}}">
                    <span class="btn-label"><i class="glyphicon glyphicon-chevron-plus"></i></span>Add New
                </a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="membership_packages_table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Membership Name</th>
                            <th>Start date</th>
                            <th>End date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg hidden">
                        <tr>
                            <th>
                                {!! Form::text('first_name', null, ["class" => "search-input-text form-control", "data-column" => 0, "placeholder" => trans('labels.backend.pages.table.title')]) !!}
                                    <a class="reset-data" href="javascript:void(0)"><i class="fa fa-times"></i></a>
                            </th>
                            <th>
                                {!! Form::select('status', [0 => "InActive", 1 => "Active"], null, ["class" => "search-input-select form-control", "data-column" => 1, "placeholder" => trans('labels.backend.pages.table.all')]) !!}
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($user_membership))
                        @foreach($user_membership as $user_membership_data)
                        <tr>
                            <td>{{\App\Models\Membership\Usermembership::get_username($user_membership_data->user_id)}}</td>
                            <td>{{\App\Models\Membership\Usermembership::get_membership_name($user_membership_data->stripe_plan)}}</td>
                            <td>{{Carbon\Carbon::parse($user_membership_data->created_at)->format('Y-m-d') }}</td>
                            <td>{{Carbon\Carbon::parse($user_membership_data->ends_at)->format('Y-m-d') }}</td>
                            <td>
                              <div class="btn-group action-btn">
                                <a href="{{route('admin.user_membership.edit',$user_membership_data->id)}}" class="btn btn-flat btn-default">
                                    <i data-toggle="tooltip" data-placement="top" title="" class="fa fa-pencil" data-original-title="Edit"></i>
                                </a>
                                                 
                                <a class="btn btn-flat btn-default" data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure you want to do this?" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
                                    <i data-toggle="tooltip" data-placement="top" title="Delete" class="fa fa-trash"></i>
                            
                                <form action="{{route('admin.user_membership.delete',$user_membership_data->id)}}" method="POST" name="delete_item" style="display:none">
                                    {{csrf_field()}}
                                   <input type="hidden" name="_method" value="delete">
                                </form>
                                </a>
                             </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <!--<div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>  /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {{-- {!! history()->renderType('CMSpage') !!} --}}
        </div><!-- /.box-body -->
    </div><!--box box-info-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        $(function() {
            var dataTable = $('#membership_packages_table').dataTable({
                 
            });

        });
    </script>
@endsection