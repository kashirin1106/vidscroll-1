@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.pages.management') . ' | ' . trans('labels.backend.pages.edit'))

@section('page-header')
    <h1>
        Membership Package Management
        <small>{{ trans('labels.backend.pages.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($membership, ['route' => ['admin.membership.update', $membership], 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'edit-role']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.pages.edit')}}</h3>

                <div class="box-tools pull-right">
                     <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.membership')}}">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>back
                     </a>
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('title', trans('validation.attributes.backend.pages.title'), ['class' => 'col-lg-2 control-label required']) }}

                    <div class="col-lg-10">
                        <input type="text" name="title" class ="form-control box-size" value="{{$membership->name}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('price', "Price", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input type="number" name= "price" class ="form-control box-size" value="{{$membership->price}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{Form::label('type','Interval',['class'=>'col-lg-2 control-label required'])}}
                    <div class="col-lg-10">
                        <select class="form-control box-size col-lg-10" name="type" required>
                            <!-- <option value="FREE" @if($membership->type == "free") selected @endif>free</option> -->
                            <option value="MONTH"  @if($membership->type == "monthly") selected @endif>monthly</option>
                            <option value="YEAR"  @if($membership->type == "yearly") selected @endif>yearly</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('enable_save_title', "Enable Save title", ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <div class="control-group">
                            <label class="control control--checkbox">
                                {{ Form::checkbox('enable_save_title', 1, ($membership->enable_save_title == 1) ? true : false ) }}
                                <div class="control__indicator"></div>
                            </label>
                        </div>
                    </div><!--col-lg-3-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('enable_save_notes', "Enable Save Note", ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <div class="control-group">
                            <label class="control control--checkbox">
                                {{ Form::checkbox('enable_save_notes', 1, ($membership->enable_save_notes == 1) ? true : false ) }}
                                <div class="control__indicator"></div>
                            </label>
                        </div>
                    </div><!--col-lg-3-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('enable_video_counts', "Enable Video Count", ['class' => 'col-lg-2 control-label required']) }}

                    <div class="col-lg-10">
                        <input type="text" name="enable_video_counts" class ="form-control box-size" value="{{$membership->enable_video_counts}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('enable_screenshot_counts', "Enable Screenshot Count", ['class' => 'col-lg-2 control-label required']) }}

                    <div class="col-lg-10">
                        <input type="text" name="enable_screenshot_counts" class ="form-control box-size" value="{{$membership->enable_screenshot_counts}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="edit-form-btn">
                    {{ link_to_route('admin.membership', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                    <div class="clearfix"></div>
                </div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
@endsection
@section("after-scripts")
    <script type="text/javascript">
        Backend.Pages.init();
    </script>
@endsection