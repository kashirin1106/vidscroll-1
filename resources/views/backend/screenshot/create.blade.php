@extends ('backend.layouts.app')

@section ('title', "User Membership" . ' | ' . trans('labels.backend.pages.create'))

@section('page-header')
    <h1>
        Screenshot
        <small>{{ trans('labels.backend.pages.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.screenshot.create', 'class' => 'form-horizontal', 'role' => 'form', 'files' => true, 'method' => 'post', 'id' => 'create-permission']) }}

    <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Create Page</h3>
                <div class="box-tools pull-right">
                     <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.screenshot')}}">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>back
                     </a>
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                
                <div class="form-group">
                    {{ Form::label('title', "Title", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="title" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                
                <div class="form-group">
					{{ Form::label('image', 'Image', ['class' => 'col-lg-2 control-label']) }}

					<div class="col-lg-10">
						{{ Form::textarea('image', null,['class' => 'form-control', 'placeholder' => 'Image', 'rows' => 2]) }}
					</div>
					<!--col-lg-3-->
				</div>
                <div class="form-group">
					{{ Form::label('detail', 'Detail', ['class' => 'col-lg-2 control-label']) }}

					<div class="col-lg-10">
						{{ Form::textarea('detail', null,['class' => 'form-control', 'placeholder' => 'Detail', 'rows' => 2]) }}
					</div>
					<!--col-lg-3-->
				</div>
                
                <div class="form-group">
                    {{ Form::label('video_name', "Video Name", ['class' => 'col-lg-2 control-label required']) }}
                    
                   <div class="col-lg-10">
                        {{ Form::text('video_name', null,['class' => 'form-control box-size', 'placeholder' => 'Video Name', 'rows' => 2]) }}
                    </div>
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('option', "Timeframe", ['class' => 'col-lg-2 control-label required']) }}
                    
                   <div class="col-lg-10">
                        {{ Form::text('option', null,['class' => 'form-control box-size', 'placeholder' => 'Detail', 'rows' => 2]) }}
                    </div>
                </div><!--form control-->
                <div class="edit-form-btn">
                    {{ link_to_route('admin.screenshot', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                    <div class="clearfix"></div>
                </div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
    @endsection
@section("after-scripts")
    <script type="text/javascript">
        Backend.Pages.init();
    </script>
@endsection
