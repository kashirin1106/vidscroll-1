@extends ('backend.layouts.app')

@section ('title', "User Screenshot Manage")

@section('page-header')
    <h1>User Screenshot Manage</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">User Screenshot Manage</h3>

            <div class="box-tools pull-right">
                <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.screenshot.add')}}">
                    <span class="btn-label"><i class="glyphicon glyphicon-chevron-plus"></i></span>Add New
                </a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="videomanager_table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Detail</th>
                            <th>Image</th>
                            <th>Video Name</th>
                            <th>Timeframe</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($screenshot))
                        @foreach($screenshot as $screenshot_data)
                        <tr>
                            <td>{{$screenshot_data->title}}</td>
                            <td style="width:250px">{!! $screenshot_data->image !!}</td>
                            <td>{!! $screenshot_data->detail !!}</td>
                            <td>{{$screenshot_data->video_name}}</td>
                            <td>{{$screenshot_data->option}}</td>
                            <td>
                              <div class="btn-group action-btn">
                                <a href="{{route('admin.screenshot.edit',$screenshot_data->id)}}" class="btn btn-flat btn-default">
                                    <i data-toggle="tooltip" data-placement="top" title="" class="fa fa-pencil" data-original-title="Edit"></i>
                                </a>
                                                 
                                <a class="btn btn-flat btn-default" data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure you want to do this?" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
                                    <i data-toggle="tooltip" data-placement="top" title="Delete" class="fa fa-trash"></i>
                            
                                <form action="{{route('admin.screenshot.delete',$screenshot_data->id)}}" method="POST" name="delete_item" style="display:none">
                                    {{csrf_field()}}
                                   <input type="hidden" name="_method" value="delete">
                                </form>
                                </a>
                             </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <!--<div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>  /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {{-- {!! history()->renderType('CMSpage') !!} --}}
        </div><!-- /.box-body -->
    </div><!--box box-info-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        $(function() {
            var dataTable = $('#videomanager_table').dataTable({
                 
            });

        });
    </script>
@endsection