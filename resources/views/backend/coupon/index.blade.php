@extends ('backend.layouts.app')

@section ('title', "User Video Manage")

@section('page-header')
    <h1>Coupon</h1>
@endsection

@section('after-styles')
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/coupon.css')}}">
@endsection

@section('content')
 
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Coupons</h3>
           
          <div class="box-header row">
            <form method="post" role="form" action="{{ route('admin.coupon.create') }}">
              {{csrf_field()}}
              <!-- <div class="col-lg-2"></div> -->
              <div class="text-center col-lg-8 col-lg-offset-3">
                  <div class="coupon_code_detail">
                    <label id="coupon_code_gen">Coupon Code(&#xf021;)</label>
                    <input type="text" name="coupon_code" id="coupon_code" class="form-control" required>
                    <!-- <button class="btn btn-primary" id="coupon_code_generate" onclick="makeid()">Generate</button> -->
                  </div>
                  <div class="coupon_code_detail">
                    <label>Free Trial Days</label>
                    <input type="number" name="coupon_code_date" max="365" class="form-control" id="coupon_code_date">
                  </div>
                  <div class="coupon_code_detail">
                    <label>Discount(%)</label>
                    <input type="number" name="coupon_code_discount" max="100" class="form-control" id="coupon_code_discount">
                  </div>
                  <div class="coupon_code_detail">
                    <button type="submit" class="btn btn-success">save</button>
                  </div>
              </div>
            </form>
          </div>
        </div><!-- /.box-header -->
 
        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="coupon_table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Coupon Code</th>
                            <th>Free Trial Days</th>
                            <th>Discount(%)</th>
                            <th>Created Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($coupon))
                        @foreach($coupon as $coupon_data)
                        <tr>
                            <td>{{$coupon_data->coupon_code}}</td>
                            <td>{{$coupon_data->period }}</td>
                            <td>{{$coupon_data->discount}}</td>
                            <td>{{$coupon_data->created_at->format('Y-m-d')}}</td>
                            <td><a class="btn btn-danger" href="{{route('admin.coupon.delete',$coupon_data->id)}}">Delete</a></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
 
@endsection
@section("after-scripts")
    {{ Html::script(mix('js/dataTable.js')) }}
  <script>
     $(function() {
            var dataTable = $('#coupon_table').dataTable({
                 
            });

        });
  </script>
  <script type="text/javascript" src="{{ asset('js/backend/coupon.js') }}"></script>
@endsection