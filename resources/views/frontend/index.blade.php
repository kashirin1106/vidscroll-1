@extends('frontend.layouts.app')

@section('after-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/index.css')}}">
@endsection

@section('content')
    <div id = "landing_page">
        <div class="col-lg-12 first_section">
            <div class="col-lg-2"></div>
            <div class="col-lg-4 text-center">
                    <div class="col-lg-12">
                        <span class="bold text-primary pull-left">{{$landingpage->tip1}}</span>
                    </div><br>
                    <div class="col-lg-12">
                        <span class="title1">{{$landingpage->title1}}</span>
                    </div>
                    <div class="col-lg-12">
                        <span class="second_title">{{$landingpage->explain1}}</span>
                    </div>    
                   
                    <div class="col-lg-12 text-center">
                        <video autoplay loop id="video-background" width="100%" height="AUTO" muted plays-inline controls>
                          <source src="{{$landingpage->video1}}" type="video/mp4">
                          Your browser does not support HTML5 video.
                        </video>
                    </div>
            </div>
            <div class="col-lg-6 text-center" id="image_triangle">
                 <div class="col-lg-12">
                    <img id="double_image" class="image" src="{{$landingpage->image1}}">
                </div>   
                 <div class="col-lg-12 text-center mt-4" id="mixpanel">
               
                    <div class="buttons row">
                        <div class="cta link_btn_group col-lg-6">
                            <a class="button button-app-store apple" href="{{ $settingData->app_store_link }}">
                                Available on the App Store
                            </a>
                        </div>
                        <div class="cta link_btn_group col-lg-6">
                            <a class="button button-app-store google" href="{{ $settingData->google_play_link }}">
                                GET IT ON GOOGLE PLAY
                            </a>
                        </div>
                    </div>
                 
                </div>
                 <div class="col-lg-12">
                        <a type="button" href="/membership/1" class="btn btn-success btn-lg free_trial_btn " id="try_free">
                        <div class="free_trial_btn_text col-lg-12 row">14 DAY FREE TRIAL</div>
                        </a>
                 </div>
            </div>
         </div>      
       
        <div class="col-lg-12 sections">
            <div class="pull-right top_button_div">
              <span class="circle_two text-primary">&#9650</span><span class="top_button"><b> TOP</b></span>
            </div>
            <div class="text-center">
                <div class="col-lg-12">
                    <span class="first_title text-primary">{{$landingpage->tip2}}</span>
                </div>
                <div class="col-lg-12">
                    <span class="col-lg-3"></span>    
                    <span class="second_title col-lg-6">
                        {{$landingpage->title2}}
                    </span>
                    <span class="col-lg-3"></span>    
                </div>
                <div class="third_title_div col-lg-12">
                    <span class="col-lg-3"></span>    
                    <span class="third_title col-lg-6">
                        {{$landingpage->explain2}}
                    </span>
                    <span class="col-lg-3"></span>    
                </div>   
                <div class="col-lg-12">
                    <img class="image" src="{{$landingpage->image2}}">
                </div> 
            </div>
        </div>

        <div class="col-lg-12 sections">
             <div class="pull-right top_button_div">
                    <span class="circle_two text-primary">&#9650</span><span class="top_button"><b> TOP</b></span>
                </div>
             <div class="col-lg-3"></div>   
            <div class="col-lg-12">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div class="col-lg-12 text-center">
                        <span class="first_title text-body text-primary">{{$landingpage->tip3}}</span>
                    </div>
                    <div class="col-lg-12 text-center">
                        <span class="second_title col-lg-12">
                            {{$landingpage->title3}}
                        </span>
                    </div>
                    <div class="third_title_div text-center">
                        <span class="third_title">
                            {{$landingpage->explain3}}
                         </span>
                    </div>   
                    <div class="col-lg-12">
                        <video autoplay loop id="video-backgrounds" width="650px" height="600px" muted plays-inline controls>
                          <source src="{{$landingpage->video3}}" type="video/mp4">
                          Your browser does not support HTML5 video.
                        </video>
                    </div> 
                </div>
               
            </div>    
            
        </div>

        <div class="col-lg-12 top_bottom">
            <div class="top_button_div pull-right" id="bottom_button">
               <span class="circle_two text-primary">&#9650</span><span class="top_button"><b> TOP</b></span>
            </div>
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="col-lg-12 text-center" id="ten_minute">
                    <span>{{$landingpage->title4}}</span>
                </div>
                <div class="col-lg-12 text-center" id="mixpanel">
               
                    <div class="buttons col-lg-6">
                        <div class="cta ">
                            <a class="button button-app-store apple" href="{{ $settingData->app_store_link }}">
                                Available on the App Store
                            </a>
                        </div>
                        <div class="cta ">
                            <a class="button button-app-store google" href="{{ $settingData->google_play_link }}">
                                GET IT ON GOOGLE PLAY
                            </a>
                        </div>
                    </div>
                    <div id="explain_text" class="col-lg-6">
                             <span>{{$landingpage->explain4}}</span>
                    </div>
                </div>
                <div class="col-lg-12 text-center" id="try_it_free">
                    <a type="button" href="/membership/1" class="btn btn-success btn-lg free_trial_btn">
                        <div class="free_trial_btn_text col-lg-12 row">14 DAY FREE TRIAL</div>
                    </a>
                </div>
            </div>    
            <div class="col-lg-3"></div>
        </div>
        <div class="col-lg-12 mt-2 mb-2" id="bottom_menu">
            <div class="text-center social_icons">
                <a href="https://www.facebook.com/sharer/sharer.php?u=http://vidscroll.vidscrollapp.com"><span class="fa fa-facebook-official"></span></a>
                <a href="https://twitter.com/intent/tweet?text=my share text&amp;url=http://vidscroll.vidscrollapp.com" class="social-button " id=""><span class="fa fa-twitter"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://vidscroll.vidscrollapp.com&amp;title=my share video&amp;summary=it is de linkedin summary" class="social-button " id=""><span class="fa fa-linkedin"></span></a>
            </div>
            <div class="text-center">
                <span>{{$landingpage->footer}}</span>
            </div>    
        </div>
        <!--<div class="col-lg-12" id="bottom_empty_blue"></div>-->
        <!--<div class="col-lg-12" id="bottom_empty_white"></div>-->
    </div>
@endsection

@section('after-scripts')
<script>
    $(document).on('click',".top_button_div", function(){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    })
   /* $(document).ready(function() {
     $('video').prop('muted',true).play()
 });*/
</script>
@endsection