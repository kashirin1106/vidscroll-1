<nav class="navbar navbar-default nav_header">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#frontend-navbar-collapse">
                <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            @if(settings()->logo)
            <a href="{{ route('frontend.index') }}" class="logo"><img height="48" width="226" class="navbar-brand" src="{{ Storage::disk('public')->url('img/logo/' . settings()->logo)  }}"></a>
            @else  
             {{ link_to_route('frontend.index',app_name(), [], ['class' => 'navbar-brand']) }}
           @endif 
        </div><!--navbar-header-->
        <div class="collapse navbar-collapse" id="frontend-navbar-collapse">
            @if ($logged_in_user)
            <ul class="nav navbar-nav">
                <!-- <li class="dropdown">
                    <a href="{{route('frontend.membership.total')}}" class="dropdown-toggle main_drop" data-toggle="dropdown" role="button" aria-expanded="false">
                        PLAN <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <?php $packages = \App\Models\Membership\MembershipPackage::get_package() ?>

                        @if(!empty($packages))
                        @foreach($packages as $packages_list)
                        <li><a href="{{route('frontend.membership.packages',$packages_list->id)}}">{{$packages_list->name}}</a></li>
                        @endforeach
                        @endif
                    </ul>
                </li> -->
                <li class="dropdown">
                    <a href="{{route('frontend.membership.total')}}" class="main_drop">
                        PLAN
                    </a>
                </li>
                <li class="dropdown">
                    <a href="{{route('frontend.feature')}}" class="main_drop">
                        DASHBOARD
                    </a>
                </li>
                 <li class="dropdown">
                    <a href="" class="dropdown-toggle main_drop" data-toggle="dropdown" role="button" aria-expanded="false">
                        HELP & LEARNING <span class="caret"></span>
                        <ul class="dropdown-menu" role="menu">
                            <li>{{ link_to_route('frontend.pages.blog', 'BLOG') }}</li>
                            <li>
                                  {{ link_to_route('frontend.pages.faq', 'FAQ') }}
                            </li>
                        </ul>
                    </a>
                 </li>   
                <li class="">
                    <a href="{{route('frontend.pages.terms')}}" class="dropdown-toggle main_drop" role="button" aria-expanded="false">
                        TERMS & CONDITIONS
                    </a>
                 </li>
            </ul> 
            @endif 
            <ul class="nav navbar-nav navbar-right">
                @if (config('locale.status') && count(config('locale.languages')) > 1)
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ trans('menus.language-picker.language') }}
                            <span class="caret"></span>
                        </a>

                        @include('includes.partials.lang')
                    </li>
                @endif

                @if ($logged_in_user)
                <?php $subscribe = \App\Models\Membership\UserMembership::get_subscribe()?>
                @if(!empty($subscribe))
                    <li>{{ link_to_route('frontend.user.dashboard', "PROFILE") }}</li>
                @endif    
                @endif

                @if (! $logged_in_user)
                    <li>{{ link_to_route('frontend.auth.login', 'LOGIN') }}</li>

                    @if (config('access.users.registration'))
                        <li>{{ link_to_route('frontend.auth.register', 'SIGN UP') }}</li>
                    @endif
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ $logged_in_user->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @permission('view-backend')
                                <li>{{ link_to_route('admin.membership', trans('navs.frontend.user.administration')) }}</li>
                            @endauth

                            <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account')) }}</li>
                            <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div><!--navbar-collapse-->
    </div><!--container-->
</nav>