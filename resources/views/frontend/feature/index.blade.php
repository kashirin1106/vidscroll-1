@extends('frontend.layouts.app')

@section ('title', "Feature")
@section('after-styles')
 <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
@endsection
@section('content')
<div class="card-deck with-border text-center">
    <div class="card-deck-body">
        <span class="header_title">MY VIDEOS</span>
    </div>
</div><!-- /.box-header -->
<hr>
<div class="container">
    <div class="box box-info">
        
        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="feature_table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Video {{ trans('labels.backend.pages.table.title') }}</th>
                            <th>Video Url</th>
                            <th>Note</th>
                            <th>Web page</th>
                            <th>Date</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                   
                    <tbody>
                        @if(!empty($video))
                        @foreach($video as $video_list)
                        <tr class="text-center">
                            <td>{{$video_list->video_title}}</td>
                            <td>{{$video_list->video_url}}</td>
                            <td>
                             {{$video_list->video_notes}}
                            </td>
                            <td>{{$video_list->web_page_url}}</td>
                            <td>{{$video_list->created_at}}</td>
                            <th><a type="button" class="btn btn-primary" href="{{route('frontend.feature.screenshot',['id'=> $video_list->video_url])}}">Screenshot</a></th>
                            <td>
                              <div class="btn-group action-btn">
                                <a href="{{route('frontend.feature.edit',$video_list->id)}}" class="btn btn-flat btn-default">
                                    <i data-toggle="tooltip" data-placement="top" title="" class="fa fa-pencil" data-original-title="Edit"></i>
                                </a>
                                                 
                                <a class="btn btn-flat btn-default" data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure you want to do this?" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
                                    <i data-toggle="tooltip" data-placement="top" title="Delete" class="fa fa-trash"></i>
                            
                                <form action="{{route('frontend.feature.delete',$video_list->id)}}" method="POST" name="delete_item" style="display:none">
                                    {{csrf_field()}}
                                   <input type="hidden" name="_method" value="delete">
                                    
                                </form>
                                </a>
                             </div>
                        </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
</div>    
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        $(function() {
            var dataTable = $('#feature_table').dataTable({
                 
            });

        });
       
    </script>
@endsection