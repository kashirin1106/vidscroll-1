@extends ('frontend.layouts.app')

@section('after-styles')
    
@endsection
@section('content')
 
    <div class="col-lg-12 card-deck">
        <div class="box-tools pull-right">
             <a type="button" class="btn btn-labeled btn-default" href="{{route('frontend.feature')}}">
                <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>back
             </a>
        </div>
        <div class="text-center card-deck-body">
            <span class="box-title header_title">{{ trans('labels.backend.pages.edit')}}</span>
        </div>
    </div><!-- /.box-header --> 
    <hr>
   <div class="container edit_page">
    {{ Form::model($video_detail, ['route' => ['frontend.feature.update', $video_detail], 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'edit-role']) }}
        <div class="box box-info">
            <div class="box-body col-lg-12">
                
                <div class="form-group">
                    {{ Form::label('membership_id', "Video Title", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="video_title" value="{{$video_detail->video_title}}" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('membership_id', "Video Url", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="video_url" value="{{$video_detail->video_url}}" readonly>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('membership_id', "Video Notes", ['class' => 'col-lg-2 control-label required']) }}
                    
                    <div class="col-lg-10">
                        <textarea class="col-lg-10 form-control box-size" name="video_notes" value="">{{$video_detail->video_notes}}</textarea>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('membership_id', "WebSite Url", ['class' => 'col-lg-2 control-label required']) }}
                    <div class="col-lg-10">
                        <input class="col-lg-10 form-control box-size" name="web_page_url" value="{{$video_detail->web_page_url}}" readonly>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="edit-form-btn text-center">
                    {{ link_to_route('frontend.feature', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                    <div class="clearfix"></div>
                </div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
</div> 
@endsection
@section("after-scripts")
    <script type="text/javascript">
        
    </script>
@endsection