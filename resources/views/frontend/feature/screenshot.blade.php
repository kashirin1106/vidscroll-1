@extends('frontend.layouts.app')

@section('after-styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/screenshot.css')}}">
@endsection

@section('content')
<div class="card-deck mt-8">
     <div class="box-tools pull-right">
         <a type="button" class="btn btn-labeled btn-default" href="{{route('frontend.feature')}}">
            <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i>back</span>
         </a>
    </div>
    <div class="container card-deck-body">
      <div class="text-center col-lg-12">
        <span class="header_title">SCREENSHOTS</span>
        <div class="resources">
            <span>Video Title:<b>{{$video_detail->video_title}}</b></span>
            <span>Video Url:{{$video_detail->video_url}}</span>
        </div>
      </div> 
    </div>
</div>
<hr>
<div class="container">
        @if(!empty($screenshot))
        @foreach($screenshot as $screenshot_list)
        <div class="card box-shadow col-lg-2">
          <img class="card-img-top" src="/img/screenshot/prototype/{{ $screenshot_list->screenshot_url }}" alt="Card image cap">
          <div class="card-body">
            <h2 class="card-title">{{$screenshot_list->name}}</h2>
            <p class="card-text">
              <ul class="list-unstyled mt-3 mb-4">
                  <h4>Time Frame:{{$screenshot_list->timeframe}}</h4>
                  <h4>Note : </h4>{{$screenshot_list->note}}
                  <h4>Date:{{$screenshot_list->created_at}}</h4>
              </ul>
            </p>
          </div>
        </div>
        @endforeach
        @endif  
        <div class="text-center">
        @if(count($screenshot) == '0')
         <h3>no screenshots you saved in this video</h3>
        @endif 
        </div>
 
</div>
@endsection