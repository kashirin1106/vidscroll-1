@extends('frontend.layouts.app')

@section('after-styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	
	<link rel="stylesheet" type="text/css" href="{{asset('css/frontend/faq.css')}}">
@endsection

@section('content')
<div class="card-deck mt-8">
	<div class="container card-deck-body">
	  <div class="text-center col-lg-12">
	  	<span class="header_title">FAQ</span>
	  </div> 
	</div>
</div>
<hr>
<div class="container demo">
    
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
        	@if(!empty($faq))
        	@foreach($faq as $faq_list)
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        {{$faq_list->question}}
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
				 {!!$faq_list->answer!!}
 				</div>
            </div>
            @endforeach
            @endif
        </div>
 
    </div><!-- panel-group -->
    
    
</div><!-- container -->
@endsection
@section('after-scripts')
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script>
	function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
	}
	$('.panel-group').on('hidden.bs.collapse', toggleIcon);
	$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
@endsection	