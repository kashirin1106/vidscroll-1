@extends('frontend.layouts.app')

@section('after-styles')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="{{asset('css/frontend/membership_total.css')}}">
@endsection

@section('content')
<div class="card-deck mt-8">
	<div class="container">
	  <div class="text-center col-lg-12">
	  	<span class="header_title">OUR BLOG</span>
	  </div> 
	</div>  
</div>
<hr>
<div class="container">
		@if(!empty($blog))
		@foreach($blog as $blog_list)
		<div class="card box-shadow col-lg-4">
		  <img class="card-img-top blog_images" src="{{Storage::disk('public')->url('img/blog/' . $blog_list->featured_image)}}" alt="Card image cap">
		  <div class="card-body">
		    <h2 class="card-title">{{$blog_list->name}}</h2>
		    <p class="card-text">
		    	<h4>{!!$blog_list->content !!}</h4>
		    </p>
		  </div>
		</div>
	     @endforeach
		@endif  
</div>
@endsection