@extends('frontend.layouts.app')

@section('after-styles')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="{{asset('css/frontend/membership_total.css')}}">
@endsection

@section('content')
<div class="card-deck mt-8">
	<div class="container card-deck-body">
	  <div class="text-center col-lg-12">
	  	<h1><b>Choose a plan</b></h1>
	  	<h3 id="solution">Find the Evernote solution that fits your needs.</h3>
		@if(!empty($membership))
		@foreach($membership as $membership_list)
	        <div class="card mb-4 box-shadow col-lg-4">
	          <div class="card-header">
	            <h1 class="my-0 font-weight-normal">{{$membership_list->name}}</h1>
	            <hr>
	          </div>
	          <div class="card-body">
	            <h1 class="card-title pricing-card-title">$0 <small class="text-muted">/ mo</small></h1>
	            <ul class="list-unstyled mt-3 mb-4">
	              <li>2 GB of storage</li>
	              <li>Email support</li>
	              <li>Help center access</li>
	            </ul>
	            <button type="button" class="btn btn-lg btn-block btn-primary">Sign up for free</button>
	          </div>
	        </div>
	     @endforeach
		@endif  
	  </div> 
	</div>
</div>

@endsection