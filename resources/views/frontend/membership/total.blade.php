@extends('frontend.layouts.app')

@section('after-styles')
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->

  <link rel="stylesheet" type="text/css" href="{{asset('css/frontend/membership_total.css')}}">
@endsection

@section('content')
<div class="card-deck mt-8">
  <div class="container card-deck-body">
    <div class="text-center col-lg-12">
      <span class="header_title">MEMBERSHIP OPTIONS</span>
      <!-- <p>FREE ONBOARDING. SCALABLE PLANS. SIMPLE RULES.</p> -->
    </div> 
  </div>
</div>
<hr>
<div class="text-center">
  <div class="tab">
      <!-- <h3 align='center'>This is a testing of Bootstrap Tabs and Pills</h3> -->
        <div class="container">
          <ul id="myTabs" class="nav nav-pills nav-justified" role="tablist" data-tabs="tabs">
            <li class="active"><a href="#monthly" data-toggle="tab">Monthly</a></li>
            <li><a href="#12_monthly" data-toggle="tab">12-month</a></li>
            <!-- <li><a href="#24_monthly" data-toggle="tab">24-month </a></li> -->
          </ul>
         
        </div>
  </div>
  <div class="main_board">
     <div class="tab-content container">
            <div role="tabpanel" class="tab-pane fade in active" id="monthly">
              <div class="tabpanel_area">
                <div class="row">
                  @if($membership_monthly)
                  @foreach($membership_monthly as $membership_monthly_list)
                   <div class="membership_packages col-lg-4 col-md-4 col-sm-4 col-4 col-xs-12">
                      <h2>{{ $membership_monthly_list->name }}</h2>
                      <p>Video Title Save : @if($membership_monthly_list->enable_save_title == 1)YES @endif</p>
                      <p>Video Title Save : @if($membership_monthly_list->enable_save_title == 1)YES @endif</p>
                      <p>Video Save Quantity : {{ $membership_monthly_list->enable_video_counts }}</p>
                      <p>Screenshot Save Quantity : {{ $membership_monthly_list->enable_video_counts }}</p>
                      @if($membership_monthly_list->type ==  "FREE")
                        <p><span class="price">Free</span></p>
                      @else
                        <p>$<span class="price">{{ $membership_monthly_list->price }}</span>/month</p>
                      @endif
                     <p>billed automatically</p>
                     <a type="button" href="{{ '/membership/'.$membership_monthly_list->id }}" class="btn btn-primary">choose package</a>
                   </div>
                  @endforeach
                  @endif 
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="12_monthly">
                <div class="tabpanel_area">
                <div class="row">
                  @if($membership_12_monthly)
                  @foreach($membership_12_monthly as $membership_12_monthly_list)
                   <div class="membership_packages col-lg-4 col-md-4 col-sm-4 col-4 col-xs-12">
                      <h2>{{ $membership_12_monthly_list->name }}</h2>
                      <p>Video Title Save : @if($membership_12_monthly_list->enable_save_title == 1)YES @endif</p>
                      <p>Video Title Save : @if($membership_12_monthly_list->enable_save_title == 1)YES @endif</p>
                      <p>Video Save Quantity : {{ $membership_12_monthly_list->enable_video_counts }}</p>
                      <p>Screenshot Save Quantity : {{ $membership_12_monthly_list->enable_video_counts }}</p>
                      @if($membership_12_monthly_list->type ==  "FREE")
                        <p><span class="price">Free</span></p>
                      @else
                        <p>$<span class="price">{{ $membership_12_monthly_list->price }}</span>/12-month</p>
                      @endif
                     <p>billed automatically</p>
                     <a type="button" href="{{ '/membership/'.$membership_12_monthly_list->id }}" class="btn btn-primary">choose package</a>
                   </div>
                  @endforeach
                  @endif 
                 
                  <!--  <div class="membership_packages col-lg-4 col-md-4 col-sm-4 col-4 col-xs-12">
                      <h2>Diamond</h2>
                     <p>$<span class="price">10.46</span>/mo</p>
                     <p>billed automatically</p>
                     <button class="btn btn-primary">choose package</button>
                   </div>
                   <div class="membership_packages col-lg-4 col-md-4 col-sm-4 col-4 col-xs-12">
                     <h2>Diamond</h2>
                     <p>$<span class="price">10.46</span>/mo</p>
                     <p>billed automatically</p>
                     <button class="btn btn-primary">choose package</button>
                   </div> -->
                </div>
              </div>
            </div>
         
          </div>
  </div>
</div>
@endsection