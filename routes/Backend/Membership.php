<?php

Route::group(['namespace' => 'Membership'], function () {

    Route::get('membership','MembershipController@index')->name('membership');
    Route::get('membership/edit/{id}','MembershipController@edit')->name('membership.edit');
    Route::post('membership_package/get', 'MembershipController@get')->name('membership_package.get');
    Route::post('membership_package/delete/{id}','MembershipController@delete')->name('membership_package.delete');

    Route::post('membership/update/{id}','MembershipController@update')->name('membership.update');

    Route::any('membership/delete/{id}','MembershipController@delete')->name('membership.delete');
    
    Route::get('membership/add','MembershipController@create')->name('membership.add');
    Route::post('membership/create','MembershipController@stores')->name('membership.create');

});

/*
 * CMS Pages Management
 */
Route::group(['namespace' => 'Membershippage'], function() {

	Route::get('membershippage','MembershippageController@edit')->name('membershippage');
	Route::post('membershippage/update','MembershippageController@update')->name('membershippage.update');

});