<?php

/*
 * CMS Pages Management
 */
Route::group(['namespace' => 'Screenshot'], function () {

    Route::get('screenshot','ScreenshotController@index')->name('screenshot');
    Route::get('screenshot/edit/{id}','ScreenshotController@edit')->name('screenshot.edit');
    Route::post('screenshot/get', 'ScreenshotController@get')->name('screenshot.get');
    // Route::post('videomanager/delete/{id}','VideoController@delete')->name('videomanager.delete');

    Route::post('screenshot/update/{id}','ScreenshotController@update')->name('screenshot.update');

    Route::any('screenshot/delete/{id}','ScreenshotController@delete')->name('screenshot.delete');
    
    Route::get('screenshot/add','ScreenshotController@create')->name('screenshot.add');
    Route::post('screenshot/create','ScreenshotController@stores')->name('screenshot.create');

});
