<?php

/*
 * CMS Pages Management
 */
Route::group(['namespace' => 'Membership'], function () {

    Route::get('user_membership','UserMembershipController@index')->name('user_membership');
    Route::get('user_membership/edit/{id}','UserMembershipController@edit')->name('user_membership.edit');
    Route::post('user_membership/get', 'UserMembershipController@get')->name('user_membership.get');
    Route::post('user_membership/delete/{id}','UserMembershipController@delete')->name('user_membership.delete');

    Route::post('user_membership/update/{id}','UserMembershipController@update')->name('user_membership.update');

    Route::any('user_membership/delete/{id}','UserMembershipController@delete')->name('user_membership.delete');
    
    Route::get('user_membership/add','UserMembershipController@create')->name('user_membership.add');
    Route::post('user_membership/create','UserMembershipController@stores')->name('user_membership.create');

});
