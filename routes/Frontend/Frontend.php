<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'FrontendController@index')->name('index');
Route::post('/get/states', 'FrontendController@getStates')->name('get.states');
Route::post('/get/cities', 'FrontendController@getCities')->name('get.cities');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

        /*
         * User Profile Picture
         */
        Route::patch('profile-picture/update', 'ProfileController@updateProfilePicture')->name('profile-picture.update');
    });

        /*
        * Show pages
        */
        Route::get('pages/{slug}', 'FrontendController@showPage')->name('pages.show');
        /*
            membership & subscription
        */
        Route::get('membership/{id}','MembershipController@index')->name('membership.packages');
        Route::get('memberships/total','MembershipController@total')->name('membership.total');
        /*
            Stripe
        */
        Route::get('/membership/stripe/{id}','StripeController@index')->name('membership.stripe');
        Route::post('/membership/plan/{id}','StripeController@plan')->name('membership.plan');
        Route::get('/plan/{plan}', 'PlanController@show')->name('plans.show');
        Route::post('/subscription', 'StripeController@create')->name('subscription.create');
        Route::get('subscription/str_cancel','StripeController@cancel')->name('subscription.str_cancel');
        Route::get('subscription/str_resume','StripeController@resume')->name('subscription.str_resume');

        /*
            paypal
        */
        Route::post('create_paypal_plan', 'PaypalController@create_plan')->name('paypal.start');
        Route::get('/subscribe/paypal', 'PaypalController@paypalRedirect')->name('paypal.redirect');
        Route::get('/subscribe/paypal/return', 'PaypalController@paypalReturn')->name('paypal.return');
        Route::get('/subscription/cancel/{agreementId}','PaypalController@cancel')->name('paypal.cancel');
        /*
            feature
        */
        Route::get('feature','FeatureController@index')->name('feature');
        Route::get('feature/edit/{id}','FeatureController@edit')->name('feature.edit');
        Route::get('feature/screenshot','FeatureController@screenshot')->name('feature.screenshot');
        Route::any('feature/delete/{id}','FeatureController@delete')->name('feature.delete');
        Route::any('feature/update/{id}','FeatureController@update')->name('feature.update');

        /*
            pages
        */
        Route::get('terms','PagesController@terms')->name('pages.terms');
        Route::get('blog','PagesController@blog')->name('pages.blog');
        Route::get('faq','PagesController@faq')->name('pages.faq');

});
        Route::get('storage/{dirname}/{sub}/{filename}', function ($dirname,$sub,$filename)
        {
            // dd("DDD");
            $path = storage_path() . '/app/public/'.$dirname.'/'.$sub. '/' . $filename;
            // dd($path);
            if(!File::exists($path)) abort(404);

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;
        });