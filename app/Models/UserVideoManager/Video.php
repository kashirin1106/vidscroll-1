<?php

namespace App\Models\UserVideoManager;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use App\Models\Page\Traits\Attribute\PageAttribute;
use App\Models\Page\Traits\PageRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends BaseModel
{
    use ModelTrait,
        SoftDeletes,
        PageRelationship,
        PageAttribute {
            // PageAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The default values for attributes.
     *
     * @var array
     */
    
    protected $with = ['owner'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "videos";
    }
}
