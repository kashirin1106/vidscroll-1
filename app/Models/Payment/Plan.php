<?php
namespace App\Models\Payment;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use App\Models\Page\Traits\Attribute\PageAttribute;
use App\Models\Page\Traits\PageRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;

class Plan extends BaseModel
{
     use ModelTrait,
     	 Billable,
         SoftDeletes,
         PageRelationship,
         PageAttribute {
            // PageAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    protected $fillable = [
        'name',
        'slug',
        'stripe_plan', 
        'cost',
        'description'
    ];
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
