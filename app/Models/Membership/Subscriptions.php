<?php

namespace App\Models\Membership;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use App\Models\Page\Traits\Attribute\PageAttribute;
use App\Models\Page\Traits\PageRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\User;
class Subscriptions extends BaseModel
{
    use ModelTrait,
        SoftDeletes,
        PageRelationship,
        PageAttribute {
            // PageAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The default values for attributes.
     *
     * @var array
     */
     
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "subscriptions";
    }

    public static function get_username($id)
    {
        $first_name = User::where('id',$id)->value('first_name');
        $last_name = User::where('id',$id)->value('last_name');

        $user_name = $first_name.' '.$last_name;
        return $user_name;
    }

    public static function get_membership_name($id)
    {
        $name = MembershipPackage::where('id',$id)->value('name');

        return $name;
    }
}
