<?php

namespace App\Notifications\Frontend\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\UserVideoManager\Video;
use App\Models\Access\User\User;
use App\Models\Feature\Screenshots;

/**
 * Class UserNeedsConfirmation.
 */
class EmailSharing extends Notification
{
    use Queueable;

    /**
     * @var
     */
    protected $video_id;
    protected $user_email;

    /**
     * UserNeedsConfirmation constructor.
     *
     * @param $confirmation_code
     */
    public function __construct($video_id,$user_email)
    {
        $this->video_id = $video_id;
        $this->user_email = $user_email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
        
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param \App\Models\Access\User\User $user
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail()
    {
        $video_data = Video::find($this->video_id);
       
        $screenshot_data = Screenshots::where('user_id',$video_data->user_id)->where('video_link',$video_data->video_url)->get();
        
        $user_email = $this->user_email;
        $user_id     = User::where('email',$this->user_email)->value('id');
        $user_firstname = User::where('id',$user_id)->value('first_name');
        $user_lastname = User::where('id',$user_id)->value('last_name');
        $user_name = $user_firstname.' '.$user_lastname;
    
         return (new MailMessage())
            ->view('emails.email-sharing',['user_name'=>$user_name, 'video_data' => $video_data, 'screenshot_data' => $screenshot_data]);
    }
}
