<?php

namespace App\Http\Controllers\Backend\Screenshot;

use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Page\Page;
use App\Repositories\Backend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Models\Membership\UserMembership;
use App\Models\Membership\MembershipPackage;
use App\Models\Access\User\User;
use App\Models\UserVideoManager\Video;
use App\Models\Screenshot\Screenshot;
/**
 * Class MembershipController.
 */
class ScreenshotController extends Controller
{
    /**
     * @param \App\Http\Requests\Backend\Pages\ManagePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index()
    {
        $screenshot = Screenshot::all();
        return view('backend.screenshot.index')->with('screenshot',$screenshot);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\CreatePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create()
    {
        $memberships = MembershipPackage::all();
        $all_users   = User::all();
        return view('backend.screenshot.create')->with('memberships',$memberships)->with('all_users',$all_users);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\StorePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function stores(Request $request)
    {
        $new_screenshot = new Screenshot;

        $new_screenshot->title          = $request->title;
        $new_screenshot->image          = $request->image;
        $new_screenshot->detail         = $request->detail;
        $new_screenshot->video_name     = $request->video_name;
        $new_screenshot->option         = $request->option;
        
        $new_screenshot->save();
        return redirect()->back()->with('flash_success','Successfully Saved!');
    }

    /**
     * @param \App\Models\Page\Page                            $page
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Page\EditResponse
     */
    public function edit(Request $request, $id)
    {
        $user_membership = UserMembership::find($id);
        $screenshot      = Screenshot::find($id);
        return view('backend.screenshot.edit')->with('user_membership',$user_membership)->with('screenshot',$screenshot);
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $screenshot = Screenshot::find($id);
        
        $screenshot->title          = $request->title;
        $screenshot->image          = $request->image;
        $screenshot->detail         = $request->detail;
        $screenshot->video_name     = $request->video_name;
        $screenshot->option         = $request->option;

        $screenshot->save();

        return redirect()->back()->with('flash_success',"Successfully updated!");
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\DeletePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function delete($id)
    {
        $screenshot = Screenshot::find($id);
        if($screenshot->delete()){
            return new RedirectResponse(route('admin.screenshot'), ['flash_success' => "Successfully deleted!"]);
        }
    }
}
