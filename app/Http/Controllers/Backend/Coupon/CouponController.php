<?php

namespace App\Http\Controllers\Backend\Coupon;

use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Page\Page;
use App\Repositories\Backend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Models\Coupon\Coupon;

/**
 * Class MembershipController.
 */
class CouponController extends Controller
{
    /**
     * @param \App\Http\Requests\Backend\Pages\ManagePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index()
    {
        $coupon = Coupon::all();
        return view('backend.coupon.index')->with('coupon',$coupon);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\CreatePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(Request $request) {
        if($check_duplicate = Coupon::where('coupon_code',$request->coupon_code)->first())
            return redirect()->back()->withFlashDanger("Coupon code can not be dupliceted!");
        if($request->coupon_code_date > 366)
            return redirect()->back()->withFlashDanger('Coupon Date can not bigger that 1 year!');
        if(is_null($request->coupon_code_date) && is_null($request->coupon_code_discount))
            return redirect()->back()->withFlashDanger('Please input free trial days or discount percents!');
            
         $new_coupon = new Coupon;
        $new_coupon->coupon_code = $request->coupon_code;
        $new_coupon->period      = $request->coupon_code_date;
        $new_coupon->discount    = $request->coupon_code_discount;
 
        if($new_coupon->save()){
            return back()->withFlashSuccess("Successfully saved!");
        }else{
            return back()->withFlashDanger("There is something wrong!");
        }
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\StorePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function stores(Request $request)
    {
        $new_membership_package = new MembershipPackage;

        $new_membership_package->name  = $request->title;
        $new_membership_package->price = $request->price;
        $new_membership_package->type  = $request->type;
        $new_membership_package->enable_save_title = $request->enable_save_title;
        $new_membership_package->enable_save_notes = $request->enable_save_notes;
        $new_membership_package->enable_video_counts = $request->enable_video_counts;
        $new_membership_package->enable_screenshot_counts = $request->enable_screenshot_counts;

        $new_membership_package->save();
        return redirect()->back();
    }

    /**
     * @param \App\Models\Page\Page                            $page
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Page\EditResponse
     */
    public function edit(Request $request, $id)
    {
        $membership = MembershipPackage::find($id);
        return view('backend.membership.edit')->with('membership',$membership)->with('flash_success','Successfully updated');
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $membership_packages = MembershipPackage::find($id);
        $membership_packages->name = $request->title;
        $membership_packages->price = $request->price;
        $membership_packages->type = $request->type;
        $membership_packages->enable_save_title = $request->enable_save_title;
        $membership_packages->enable_save_notes = $request->enable_save_notes;
        $membership_packages->enable_video_counts = $request->enable_video_counts;
        $membership_packages->enable_screenshot_counts = $request->enable_screenshot_counts;

        $membership_packages->save();

        return redirect()->back()->with('flash_success',"Successfully updated!");
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\DeletePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function delete($id)
    {
        $coupon_code = Coupon::find($id);
        if($coupon_code->delete()){
            return new RedirectResponse(route('admin.coupon'), ['flash_success' => "Successfully deleted!"]);
        }
    }
}
