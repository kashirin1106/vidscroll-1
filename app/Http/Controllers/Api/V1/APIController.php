<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response as IlluminateResponse;
use Response;

/**
 * Base API Controller.
 */
class APIController extends Controller
{
    /**
     * default status code.
     *
     * @var int
     */
    protected $statusCode = 200;

    /**
     * get the status code.
     *
     * @return statuscode
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * set the status code.
     *
     * @param [type] $statusCode [description]
     *
     * @return statuscode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Respond.
     *
     * @param array $data
     * @param array $headers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * respond with pagincation.
     *
     * @param Paginator $items
     * @param array     $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithPagination($items, $data)
    {
        $data = array_merge($data, [
            'paginator' => [
                'total_count'  => $items->total(),
                'total_pages'  => ceil($items->total() / $items->perPage()),
                'current_page' => $items->currentPage(),
                'limit'        => $items->perPage(),
             ],
        ]);

        return $this->respond($data);
    }

    /**
     * Respond Created.
     *
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondCreated($data)
    {
        return $this->setStatusCode(201)->respond([
            'data' => $data,
        ]);
    }

    /**
     * Respond Created with data.
     *
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondCreatedWithData($data)
    {
        return $this->setStatusCode(201)->respond($data);
    }

    /**
     * respond with error.
     *
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithError($message)
    {
        return $this->respond([
                'error' => [
                    'message'     => $message,
                    'status_code' => $this->getStatusCode(),
                ],
            ]);
    }

    /**
     * responsd not found.
     *
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($message);
    }

    /**
     * Respond with error.
     *
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondInternalError($message = 'Internal Error')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }

    /**
     * Respond with unauthorized.
     *
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondUnauthorized($message = 'Unauthorized')
    {
        return $this->setStatusCode(401)->respondWithError($message);
    }

    /**
     * Respond with forbidden.
     *
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondForbidden($message = 'Forbidden')
    {
        return $this->setStatusCode(403)->respondWithError($message);
    }

    /**
     * Respond with no content.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithNoContent()
    {
        return $this->setStatusCode(204)->respond(null);
    }

    /**Note this function is same as the below function but instead of responding with error below function returns error json
     * Throw Validation.
     *
     * @param string $message
     *
     * @return mix
     */
    public function throwValidation($message)
    {
        return $this->setStatusCode(422)
            ->respondWithError($message);
    }


    public function api()
    {
        $json = '{"id":3,"email":"rnFnPcH@gmail.com","firstName":"Keren","lastName":"Shalom UKPNfQqNoQ","dob":"2019-03-03T16:52:05.540000Z","street":"15 RiverColonial Cr","phone":"2030754927898","city":"Saint John","country":"CA","zipCode":"E2K 2K1","title":"Mr","stepId":1,"locale":"en","salesPersonName":"Elad Shalom","created":"2019-03-04T00:52:05.641000Z","updated":"2019-03-04T00:52:05.641000Z","lastReached":"2019-03-04T00:52:05.641000Z","password":"AsePICBwkbpbJvxHIxbW","roles":[{"name":"CUSTOMER"}],"products":[{"id":1,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.630000Z","updated":"2019-03-04T00:52:04.709000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.482000Z","cost":200}],"name":"a new one!","purchaseDateTime":"2019-03-04T00:52:05.641000Z","purchaseId":543191824087384,"salesPersonName":"Johnny Sales RrUFL"},{"id":1,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.630000Z","updated":"2019-03-04T00:52:04.709000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.482000Z","cost":200}],"name":"a new one!","purchaseDateTime":"2019-03-04T00:52:05.641000Z","purchaseId":506788616544089,"salesPersonName":"Johnny Sales BRLSq"},{"id":3,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.678000Z","updated":"2019-03-04T00:52:04.678000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.674000Z","cost":200}],"name":"Amazing Cruise nuQNExpfVI","purchaseDateTime":"2019-03-04T00:52:05.641000Z","purchaseId":350724919972467,"salesPersonName":"Johnny Sales EJOAh"},{"id":3,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.678000Z","updated":"2019-03-04T00:52:04.678000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.674000Z","cost":200}],"name":"Amazing Cruise nuQNExpfVI","purchaseDateTime":"2019-03-04T00:52:05.641000Z","purchaseId":175683367692303,"salesPersonName":"Johnny Sales SIIJA"},{"id":1,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.630000Z","updated":"2019-03-04T00:52:04.709000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.482000Z","cost":200}],"name":"a new one!","purchaseDateTime":"2019-03-04T00:52:05.641000Z","purchaseId":380334969935583,"salesPersonName":"Johnny Sales msYLV"},{"id":1,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.630000Z","updated":"2019-03-04T00:52:04.709000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.482000Z","cost":200}],"name":"a new one!","purchaseDateTime":"2019-03-04T00:52:05.641000Z","purchaseId":236336707966849,"salesPersonName":"Johnny Sales KLVSe"},{"id":3,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.678000Z","updated":"2019-03-04T00:52:04.678000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.674000Z","cost":200}],"name":"Amazing Cruise nuQNExpfVI","purchaseDateTime":"2019-03-04T00:52:05.641000Z","purchaseId":496184693218831,"salesPersonName":"Johnny Sales kFqVi"},{"id":3,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.678000Z","updated":"2019-03-04T00:52:04.678000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.674000Z","cost":200}],"name":"Amazing Cruise nuQNExpfVI","purchaseDateTime":"2019-03-04T00:52:05.641000Z","purchaseId":120912185294852,"salesPersonName":"Johnny Sales OnISH"},{"id":1,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.630000Z","updated":"2019-03-04T00:52:04.709000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.482000Z","cost":200}],"name":"a new one!","purchaseDateTime":"2019-03-04T00:52:05.641000Z","purchaseId":498992734533845,"salesPersonName":"Johnny Sales iZBSL"}],"productsOfInterest":[{"id":1,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.630000Z","updated":"2019-03-04T00:52:04.709000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.482000Z","cost":200}],"name":"a new one!"},{"id":3,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.678000Z","updated":"2019-03-04T00:52:04.678000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.674000Z","cost":200}],"name":"Amazing Cruise nuQNExpfVI"},{"id":3,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.678000Z","updated":"2019-03-04T00:52:04.678000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.674000Z","dateTimeTo":"2019-03-03T16:52:04.674000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.674000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.674000Z","cost":200}],"name":"Amazing Cruise nuQNExpfVI"},{"id":2,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.659000Z","dateTimeTo":"2019-03-03T16:52:04.659000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.664000Z","updated":"2019-03-04T00:52:04.664000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.659000Z","dateTimeTo":"2019-03-03T16:52:04.659000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.659000Z","dateTimeTo":"2019-03-03T16:52:04.659000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.659000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.659000Z","cost":200}],"name":"SoSo Tours XroPkvzwCz"},{"id":1,"type":"Cruise","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","origin":"Saint John NB","highlights":"some markers","promotionalDesc":"promote!","itinerary":"http://www.domain.com/file.pdf","video":"http://youtoube.com/123","inclusions":"<html></html>","priceDetails":"some price details","totalCost":5000,"totalPaid":0,"created":"2019-03-04T00:52:04.630000Z","updated":"2019-03-04T00:52:04.709000Z","accommodations":[{"vendor":"Hilton","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info........","rooms":[{"type":"Executive","availNum":4,"cost":2000,"additionalTravelers":5}]}],"transportation":[{"vendor":"Egged","dateTimeFrom":"2019-03-03T16:52:04.482000Z","dateTimeTo":"2019-03-03T16:52:04.482000Z","additionalInfo":"Just some info","seats":[{"type":"Economy","availNum":5,"cost":100}]}],"flights":[{"vendor":"Air Canada","origin":"YSJ","departure":"2019-03-03T16:52:04.482000Z","additionalInfo":"some info","seats":[{"type":"Business","availNum":5,"cost":400}]}],"pics":[{"name":"mypic.jpg","url":"http://www.domain.com/mypic.jpg","order_by":4}],"paymentSteps":[{"type":"deposit","due":"2019-03-03T16:52:04.482000Z","cost":200}],"name":"a new one!"}]}';
        return $json;
    }
}
