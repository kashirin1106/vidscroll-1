<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Membership\MembershipPackage;
use App\Models\Membershippage\Membershippage;

/**
 * Class FrontendController.
 */
class MembershipController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index($id)
    {
        $membership = MembershipPackage::where('id',$id)->first();
        $membershippage = Membershippage::first();

        return view('frontend.membership.membership', compact('membership','membershippage'));
    }
    

    public function silver()
    {
        $membership = MembershipPackage::where('id','1')->first();
        return view('frontend.membership.membership',compact('membership'));
    }

    public function total()
    {   
        
        $membership_monthly = MembershipPackage::where('type','MONTH')->orWhere('type','FREE')->get();
        $membership_12_monthly = MembershipPackage::whereIn('type',['YEAR','FREE'])->get();
        $membership_list = MembershipPackage::all();
        
        return view('frontend.membership.total',compact('membership_list','membership_monthly','membership_12_monthly'));
    }

}
