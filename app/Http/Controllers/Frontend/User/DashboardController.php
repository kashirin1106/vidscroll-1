<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\DashboardViewRequest;
use App\Models\Membership\Subscriptions;
use App\Models\Membership\MembershipPackage;
use App\Models\Feature\Screenshots;
use App\Models\UserVideoManager\Video;
/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(DashboardViewRequest $request)
    {
         $user_id = auth()->user()->id;
         if(!Subscriptions::where('user_id',$user_id)->first())
            return redirect('/');
         
    	 $current_videos = Video::where('user_id',$user_id)->count();
         $current_screens = Screenshots::where('user_id',$user_id)->count();
         
        return view('frontend.user.dashboard',compact('current_videos','current_screens'));
    }
}
