<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Membership\MembershipPackage;
use App\Models\Feature\Screenshots;
use App\Models\Payment\Plan;
use App\Models\Blogs\Blog;
use App\Models\Faqs\Faq;

/**
 * PlanController
 */
class PagesController extends Controller
{
	public function terms()
	{
        $terms = Blog::all();
        return view('frontend.pages.terms', compact('terms'));
	}

	/*public function membership($id)
	{
		$membership = MembershipPackage::where('id',$id)->first();
		return view('')
	}*/
	public function blog()
	{
		$blog = Blog::all();
	     return view('frontend.pages.blog', compact('blog'));
	}

	public function faq()
	{
		$faq = Faq::where('status',1)->get();
		return view('frontend.pages.faq',compact('faq'));
	}
}
