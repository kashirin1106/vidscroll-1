<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Membership\MembershipPackage;
use App\Models\Feature\Screenshots;
use App\Models\Payment\Plan;

/**
 * PlanController
 */
class PlanController extends Controller
{
	
	/*function __construct(argument)
	{
		# code...
	}*/

	public function index()
	{
        $plans = Plan::all();
        return view('plans.index', compact('plans'));
	}

	/*public function membership($id)
	{
		$membership = MembershipPackage::where('id',$id)->first();
		return view('')
	}*/
	public function show(Plan $plan, Request $request)
	{
	     return view('plans.show', compact('plan'));
	}
}
