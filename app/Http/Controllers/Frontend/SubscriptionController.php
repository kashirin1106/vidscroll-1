<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Models\Notification\Membership;
use Illuminate\View\Middleware\ShareErrorsFromSession;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Api\Payer;
use PayPal\Api\ShippingAddress;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use App\Models\Membership\Subscriptions;

class SubscriptionController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public $_api_context;
    public $budget;
    public $membership_id;
    public $agreementId;

    public function __construct()
    {
/** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        // dd($paypal_conf);
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'] 
            )
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
     
    public function subcreate(Request $request)
    {

        $validatedData = $request->validate([
            'budget' => 'required',
            'membership_id' => 'required',
            // 'type' => 'required',
        ]);

        //   if ($validator->fails()) {
        //     return redirect('post/create')
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }
        $membership_id = $request->membership_id;
        $this->membership_id = $membership_id;
        $budget = $request->budget;
        // $years = $request->years;
        // $type = $request->type;
        $this->budget = $budget;

        $subDescription = "My plan:".$membership_id.":".$budget;
        // dd($subDescription);
        $start_dates = Date('Y-m-d H:i:s');
        // dd($start_dates);
        $plan = new Plan();
        $plan->setName('Monthly Sub Plan')
        ->setDescription($subDescription)
        ->setType('fixed');

        $paymentDefinition = new PaymentDefinition();

        $paymentDefinition->setName('Regular Payments')
        ->setType('REGULAR')
        ->setFrequency('Month')
        ->setFrequencyInterval("12")
        ->setCycles("12")
        ->setAmount(new Currency(array('value' => 100, 'currency' => 'USD')));    

        $chargeModel = new ChargeModel();
        $chargeModel->setType('SHIPPING')
        ->setAmount(new Currency(array('value' => 10, 'currency' => 'USD')));

        $paymentDefinition->setChargeModels(array($chargeModel));

        $merchantPreferences = new MerchantPreferences();
        $baseUrl = url()->current();

        $merchantPreferences->setReturnUrl("http://localhost:8000/paypal/success")
        ->setCancelUrl("http://localhost:8000/user/account")
        ->setAutoBillAmount("yes")
        ->setInitialFailAmountAction("CONTINUE")
        ->setMaxFailAttempts("0")
        ->setSetupFee(new Currency(array('value' => $budget, 'currency' => 'USD')));
        // dd($merchantPreferences);
        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);
        // dd($plan);
        $request = clone $plan;

        try {
            $output = $plan->create($this->_api_context);
            // dd("dd",$this->_api_context);
        } catch (Exception $ex) {
            
            exit(1);
        }
        $output;
        try {
        $patch = new Patch();

        $value = new PayPalModel('{
               "state":"ACTIVE"
             }');

        $patch->setOp('replace')
            ->setPath('/')
            ->setValue($value);
        $patchRequest = new PatchRequest();
        $patchRequest->addPatch($patch);

        $plan->update($patchRequest, $this->_api_context);

        $plan = Plan::get($plan->getId(), $this->_api_context);
        // dd($plan,$this->_api_context,$plan->getId());
        } catch (Exception $ex) {
            // ResultPrinter::printError("Updated the Plan to Active State", "Plan", null, $patchRequest, $ex);
            exit(1);
        }
        // dd($plan);
        // ResultPrinter::printResult("Updated the Plan to Active State", "Plan", $plan->getId(), $patchRequest, $plan);
         $finalplan =  $plan;  

        $agreement = new Agreement();

        $agreement->setName('Base Agreement')
            ->setDescription('Basic Agreement')
            ->setStartDate('2019-06-17T9:45:04Z');
            // $plan = new Plan();
        $plan = new Plan();

        $plan->setId($finalplan->getId());
        $agreement->setPlan($plan);
        // $agreement->setrequestid('B-5YW327438T794174S');
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);
        // dd($agreement);
        $shippingAddress = new ShippingAddress();
        $shippingAddress->setLine1('111 First Street')
            ->setLine2($subDescription)
            ->setCity('Saratoga')
            ->setState('CA')
            ->setPostalCode('95070')
            ->setCountryCode('US');
        $agreement->setShippingAddress($shippingAddress);

        $request = clone $agreement;
        try {
            // dd($this->_api_context->getRequestid());
                $agreement = $agreement->create($this->_api_context);
                // $token     = $this->getPayPalTokenFromUrl($agreement->getApprovalLink());
                $approvalUrl = $agreement->getApprovalLink();
                // dd($approvalUrl);

        } catch (Exception $ex) {
             // ResultPrinter::printError("Created Billing Agreement.", "Agreement", null, $request, $ex);
            exit(1);
        }
        return redirect($approvalUrl);
    }
    
    public function success(Request $request){
        
        $token = $request->query('token');
        if(!$token) dd("user canceled pay");
        $agreement = new \PayPal\Api\Agreement();
        try {
            // ## Execute Agreement
            // Execute the agreement by passing in the token
            $agreement->execute($token, $this->_api_context);
        } catch (Exception $ex) {
            exit(1);
        }
        // ## Get Agreement
        // Make a get call to retrieve the executed agreement details
        try {
            $agreement = \PayPal\Api\Agreement::get($agreement->getId(),  $this->_api_context);
        } catch (Exception $ex) {
            exit(1);
        }
         $agreementId = $agreement->getId();
            $params = array('start_date' => date('Y-m-d', strtotime('-15 years')), 'end_date' => date('Y-m-d', strtotime('+5 days')));
            try {
                $result = \PayPal\Api\Agreement::searchTransactions($agreementId, $params, $this->_api_context);
            } catch (Exception $ex) {
                 exit(1);
        }
        $this->agreementId = $agreementId;
        
        $detail = explode(':', $agreement->shipping_address->line2);
        
        // dd($result,"kk",$agreement,$agreement->shipping_address->line2);
        // dd($subscrribetype,$subscrribeyear,$subscrribebudget);
        // dd($result->agreement_transaction_list[0]->transaction_id);
       if($result->agreement_transaction_list){
        $rr = date('Y-m-d H:i:s', strtotime('+1 month'));
        
        $userid = auth()->user()->id;
        
        $membership = new Subscriptions;

        $membership ->gateway = 1;
        $membership ->user_id = $userid;
        $membership ->name = "main";
        $membership ->stripe_id = $agreementId;
        $membership ->stripe_plan = $detail[1];
        $membership ->ends_at = $rr;
         
        $membership->save();
        // return redirect('/bucket/create');
        return redirect()->back();
      }
    }
// $result->agreement_transaction_list[1]->transaction_id,$result->agreement_transaction_list[1]->amount->value,$result->agreement_transaction_list[1]->time_stamp
    public function cancel($agreementId)
    {
        $agreement = new \PayPal\Api\Agreement();            

        $agreement->setId($agreementId);
        $agreementStateDescriptor = new \PayPal\Api\AgreementStateDescriptor();
        $agreementStateDescriptor->setNote("Cancel the agreement");

        try {
            $agreement->cancel($agreementStateDescriptor, $this->_api_context);
            $cancelAgreementDetails = \PayPal\Api\Agreement::get($agreement->getId(), $this->_api_context);   
            $status = Subscriptions::where('stripe_id',$agreementId)->first();
            $status->status = "canceled";
            $status->save();

            return redirect()->route('frontend.user.account')->withFlashDanger("Membership has been canceled!");

        } catch (Exception $ex) {    

            return redirect()->back()->withFlashDanger("Server error!");
            
        }
    }
  
  }
